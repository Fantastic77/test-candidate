<?php
namespace modules\candidate;
/**
 * site module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'modules\candidate\controllers\frontend';
    /**
     * @var boolean.
     */
    public $isBackend;
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        if ($this->isBackend === true) {
            $this->controllerNamespace = 'modules\candidate\controllers\backend';
            $this->setViewPath('@modules\candidate/views/backend');
        } else {
            $this->setViewPath('@modules/candidate/views/frontend');
        }
    }
}