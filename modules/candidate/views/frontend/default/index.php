<?php
//$this->title = \modules\seo\helpers\Meta::all('candidate', $model);
?>

<div class="candidate-default-index" data-ng-controller="CandidateController">
    <div>

        <button type="button" class="btn btn-success btn-md" data-toggle="modal" data-target="#myModal">Add candidate</button>

        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add Candidate</h4>
                    </div>
                    <div class="modal-body">
                       <form>
                           <input ng-model="candidate.name"><br/>
                           <input ng-model="candidate.birthday"><br/>
                           <input ng-model="candidate.experience"><br/>
                           <input ng-model="candidate.comment"><br/>
                           <button type="button" class="btn btn-success" ng-click="create(candidate)" data-dismiss="modal" >Create</button>
                       </form>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

        <h1>All Candidates</h1>
        <div data-ng-show="candidates.length > 0">
            <table class="table table-striped table-hover">
                <thead>
                <th>Id</th>
                <th>Name</th>
                <th>Birthday</th>
                <th>Experience</th>
<!--                <th>Frameworks</th>-->
                <th>Comment</th>
                <th>Created at</th>
                <th>Edit</th>
                <th>Delete</th>
                </thead>
                <tbody>
                <tr data-ng-repeat="candidate in candidates">
                    <td>{{candidate.id}}</td>
                    <td>{{candidate.name}}</td>
                    <td>{{candidate.birthday}}</td>
                    <td>{{candidate.experience}}</td>
<!--                    <td>Frameworks</td>-->
                    <td>{{candidate.comment}}</td>
                    <td>{{candidate.created_at}}</td>
                    <td><a href="#">Edit</a></td>
                    <td><a ng-click="delete(candidate.id)">Delete</a></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div data-ng-show="candidates.length == 0">
            No results
        </div>
    </div>
</div>