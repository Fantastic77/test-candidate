<?php
namespace modules\candidate;
use yii\base\BootstrapInterface;
class Bootstrap implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules(
            [
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['candidates' => 'candidate/api'],
                    'prefix' => 'api'
                ]
            ]
        );
    }
}