<?php
namespace modules\candidate\models\frontend;
use yii\db\ActiveRecord;
use Yii;
/**
 * Class Candidate
 * @package modules\candidate\models\frontend
 * Candidate model.
 *
 * This is the model class for table "{{%candidate}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $birthday
 * @property int $experience
 * @property string $comment
 * @property string $created_at
 *
 * @property CandidateFramework[] $candidateFramworks
 * @property Framework[] $frameworks
 */
class Candidate extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%candidate}}';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['birthday', 'created_at'], 'safe'],
            [['experience'], 'integer'],
            [['name', 'comment'], 'string', 'max' => 255],
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'birthday' => 'Birthday',
            'experience' => 'Experience',
            'comment' => 'Comment',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCandidateFrameworks()
    {
        return $this->hasMany(CandidateFramework::className(), ['candidate_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFrameworks()
    {
        return $this->hasMany(Framework::className(), ['id' => 'framework_id'])->viaTable('candidate_framwork', ['candidate_id' => 'id']);
    }
}