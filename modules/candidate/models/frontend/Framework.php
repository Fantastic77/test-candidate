<?php

namespace modules\candidate\models\frontend;
use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "framework".
 *
 * @property int $id
 * @property string $title
 *
 * @property CandidateFramework[] $candidateFrameworks
 * @property Candidate[] $candidates
 */
class Framework extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%framework}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCandidateFrameworks()
    {
        return $this->hasMany(CandidateFramework::className(), ['framework_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCandidates()
    {
        return $this->hasMany(Candidate::className(), ['id' => 'candidate_id'])->viaTable('candidate_framework', ['framework_id' => 'id']);
    }
}
