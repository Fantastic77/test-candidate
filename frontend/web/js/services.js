'use strict';

var app = angular.module('app');

app.service('CandidateService', function($http) {
    this.get = function() {
        return $http.get('/api/candidates');
    };
    this.post = function (data) {
        return $http.post('/api/candidates', data);
    };
    this.put = function (id, data) {
        return $http.put('/api/candidates/' + id, data);
    };
    this.delete = function (id) {
        return $http.delete('/api/candidates/' + id);
    };
});