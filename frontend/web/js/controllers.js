'use strict';

var controllers = angular.module('controllers', []);

controllers.controller('CandidateController', [
    '$scope',
    'CandidateService',
    function (
        $scope,
        CandidateService
    ) {
            var controller = this;

            $scope.candidates = [];
            CandidateService.get().then(function (data) {
                if (data.status == 200)
                    $scope.candidates = data.data;
            }, function (err) {
                console.log(err);
            });

            $scope.delete = function(id) {
                CandidateService.delete(id).then(function (data) {
                    $scope.candidates = [];
                    CandidateService.get().then(function (data) {
                        if (data.status == 200)
                            $scope.candidates = data.data;
                    }, function (err) {
                        console.log(err);
                    });
                }, function (err) {
                    console.log(err);
                });
            };

            $scope.create = function(candidate) {
                console.log(candidate);
                CandidateService.post(candidate).then(function(data) {
                    $scope.candidates = [];
                    CandidateService.get().then(function (data) {
                        if (data.status == 200)
                            $scope.candidates = data.data;
                    }, function (err) {
                        console.log(err);
                    });
                });
            }
        }
]);